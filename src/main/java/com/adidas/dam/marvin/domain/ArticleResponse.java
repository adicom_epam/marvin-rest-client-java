package com.adidas.dam.marvin.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleResponse {
	
	@JsonProperty("Count")
	private long count;

	@JsonProperty("NextCursorMark")
	private String nextCursorMark;

	@JsonProperty("Results")
	private List<Article> articles;
	
	/**
	 * @return the count
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @return the next cursor mark
	 */
	public String getNextCursorMark() {
		return nextCursorMark;
	}

	/**
	 * @return the articles
	 */
	public List<Article> getArticles() {
		return articles;
	}
	
	
}
