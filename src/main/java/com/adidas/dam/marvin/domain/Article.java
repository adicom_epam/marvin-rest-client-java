package com.adidas.dam.marvin.domain;

import com.adidas.dam.marvin.util.ArrayUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Article extends Entity {

    public static final String ARTICLE_NAME = "ArticleName";
    public static final String ARTICLE_NUMBER = "ArticleNumber";
    public static final String PRODUCT_DIVISIONS = "ProductDivisions";
    public static final String PRODUCT_MANAGERS = "ProductManagers";

    @JsonProperty(ARTICLE_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String name;

    @JsonProperty(ARTICLE_NUMBER)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String number;

    @JsonProperty(PRODUCT_DIVISIONS)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String[] productDivisions;

    @JsonProperty(PRODUCT_MANAGERS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] productManagers;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @return the productDivisions
     */
    public String[] getProductDivisions() {
        return ArrayUtil.copyOf(productDivisions);
    }

    /**
     * @return the productManagers
     */
    public String[] getProductManagers() {
        return ArrayUtil.copyOf(productManagers);
    }

}
