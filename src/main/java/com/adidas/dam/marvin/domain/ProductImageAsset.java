package com.adidas.dam.marvin.domain;

import com.adidas.dam.marvin.util.ArrayUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

// disabling too many fields warning since there is no way further
// separting this class
@SuppressWarnings("TooManyFields")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductImageAsset extends Asset {

    public static final String MODEL_NUMBER = "ModelNumber";
    public static final String MODEL_NAME = "ModelName";
    public static final String WORKING_NUMBER = "WorkingNumber";
    public static final String ARTICLE_NAME = "ArticleName";
    public static final String DESIGN_BOARD_NAME = "DesignBoardName";
    public static final String DEVELOPER = "Developer";
    public static final String DESIGN_FILE_NAME = "DesignFileName";
    public static final String MAIN_RGB_COLOR = "MainRgbColor";
    public static final String COLOR_COMBINATION = "ColorCombination";
    public static final String PLM_FILE_NAME = "PlmFileName";
    public static final String COMMENTS = "Comments";
    public static final String ARTICLE_NUMBER = "ArticleNumber";
    public static final String AFS_FLAGS = "AfsFlags";
    public static final String SELLING_SEASONS = "SellingSeasons";
    public static final String ASSET_CATEGORY = "AssetCategory";
    public static final String PRODUCT_VIEW = "ProductView";
    public static final String STATUS = "Status";
    public static final String VENDOR = "Vendor";
    public static final String IMAGE_STYLE = "ImageStyle";
    public static final String ASSET_PHASE = "AssetPhase";
    public static final String APPROVED_BY = "ApprovedBy";
    public static final String PRODUCT_DIVISIONS = "ProductDivisions";
    public static final String SCOPES = "Scopes";
    public static final String AGE_GROUPS = "AgeGroups";
    public static final String PRODUCT_TYPES = "ProductTypes";
    public static final String GENDERS = "Genders";
    public static final String PRODUCT_MANAGERS = "ProductManagers";
    public static final String USAGE = "Usage";
    public static final String MARKETING_SEASONS = "MarketingSeasons";
    public static final String IMAGE_EXPIRY_DATE = "ImageExpiryDate";
    public static final String LATEST_FILE_DOWNLOAD_PARTITION_URL = "LatestFileDownloadPartitionUrl";
    public static final String PRODUCT_SIZE = "ProductSize";
    public static final String MODEL_HEIGHT = "ModelHeight";
    public static final String MODEL_WAIST = "ModelWaist";
    public static final String MODEL_CHEST = "ModelChest";
    public static final String TECHNICAL_SIZE = "TechnicalSize";

    @JsonProperty(MODEL_NUMBER)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String modelNumber;

    @JsonProperty(MODEL_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String modelName;

    @JsonProperty(WORKING_NUMBER)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String workingNumber;

    @JsonProperty(ARTICLE_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String articleName;

    @JsonProperty(DESIGN_BOARD_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String designBoardName;

    @JsonProperty(DEVELOPER)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String developer;

    @JsonProperty(DESIGN_FILE_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String designFileName;

    @JsonProperty(MAIN_RGB_COLOR)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String mainRgbColor;

    @JsonProperty(COLOR_COMBINATION)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String colorCombination;

    @JsonProperty(PLM_FILE_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String plmFileName;

    @JsonProperty(COMMENTS)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String comments;

    @JsonProperty(ARTICLE_NUMBER)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String articleNumber;

    @JsonProperty(AFS_FLAGS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] afsFlags;

    @JsonProperty(SELLING_SEASONS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] sellingSeasons;

    @JsonProperty(ASSET_CATEGORY)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String assetCategory;

    @JsonProperty(PRODUCT_VIEW)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String productView;

    @JsonProperty(STATUS)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String status;

    @JsonProperty(VENDOR)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String vendor;

    @JsonProperty(IMAGE_STYLE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String imageStyle;

    @JsonProperty(ASSET_PHASE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String assetPhase;

    @JsonProperty(APPROVED_BY)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String approvedBy;

    @JsonProperty(PRODUCT_DIVISIONS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] productDivisions;

    @JsonProperty(SCOPES)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] scopes;

    @JsonProperty(AGE_GROUPS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] ageGroups;

    @JsonProperty(PRODUCT_TYPES)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] productTypes;

    @JsonProperty(GENDERS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] genders;

    @JsonProperty(PRODUCT_MANAGERS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] productManagers;

    @JsonProperty(USAGE)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] usage;

    @JsonProperty(MARKETING_SEASONS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] marketingSeasons;

    @JsonProperty(IMAGE_EXPIRY_DATE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("DateTime")
    private DateTime imageExpiryDate;

    @JsonProperty(LATEST_FILE_DOWNLOAD_PARTITION_URL)
    @Filterable
    @Selectable
    @ODataType("string")
    private String latestFileDownloadPartitionUrl;

    @JsonProperty(PRODUCT_SIZE)
    @Filterable
    @Selectable
    @ODataType("string")
    private String productSize;

    @JsonProperty(MODEL_HEIGHT)
    @Filterable
    @Selectable
    @ODataType("string")
    private String modelHeight;

    @JsonProperty(MODEL_WAIST)
    @Filterable
    @Selectable
    @ODataType("string")
    private String modelWaist;

    @JsonProperty(MODEL_CHEST)
    @Filterable
    @Selectable
    @ODataType("string")
    private String modelChest;

    @JsonProperty(TECHNICAL_SIZE)
    @Filterable
    @Selectable
    @ODataType("string")
    private String technicalSize;

    /**
     * @return the modelNumber
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * @return the workingNumber
     */
    public String getWorkingNumber() {
        return workingNumber;
    }

    /**
     * @return the articleName
     */
    public String getArticleName() {
        return articleName;
    }

    /**
     * @return the designBoardName
     */
    public String getDesignBoardName() {
        return designBoardName;
    }

    /**
     * @return the developer
     */
    public String getDeveloper() {
        return developer;
    }

    /**
     * @return the designFileName
     */
    public String getDesignFileName() {
        return designFileName;
    }

    /**
     * @return the mainRgbColor
     */
    public String getMainRgbColor() {
        return mainRgbColor;
    }

    /**
     * @return the colorCombination
     */
    public String getColorCombination() {
        return colorCombination;
    }

    /**
     * @return the plmFileName
     */
    public String getPlmFileName() {
        return plmFileName;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @return the articleNumber
     */
    public String getArticleNumber() {
        return articleNumber;
    }

    /**
     * @return the afsFlags
     */
    public String[] getAfsFlags() {
        return ArrayUtil.copyOf(afsFlags);
    }

    /**
     * @return the sellingSeasons
     */
    public String[] getSellingSeasons() {
        return ArrayUtil.copyOf(sellingSeasons);
    }

    /**
     * @return the assetCategory
     */
    public String getAssetCategory() {
        return assetCategory;
    }

    /**
     * @return the productView
     */
    public String getProductView() {
        return productView;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * @return the imageStyle
     */
    public String getImageStyle() {
        return imageStyle;
    }

    /**
     * @return the assetPhase
     */
    public String getAssetPhase() {
        return assetPhase;
    }

    /**
     * @return the approvedBy
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * @return the productDivisions
     */
    public String[] getProductDivisions() {
        return ArrayUtil.copyOf(productDivisions);
    }

    /**
     * @return the scopes
     */
    public String[] getScopes() {
        return ArrayUtil.copyOf(scopes);
    }

    /**
     * @return the ageGroups
     */
    public String[] getAgeGroups() {
        return ArrayUtil.copyOf(ageGroups);
    }

    /**
     * @return the productTypes
     */
    public String[] getProductTypes() {
        return ArrayUtil.copyOf(productTypes);
    }

    /**
     * @return the genders
     */
    public String[] getGenders() {
        return ArrayUtil.copyOf(genders);
    }

    /**
     * @return the productManagers
     */
    public String[] getProductManagers() {
        return ArrayUtil.copyOf(productManagers);
    }

    /**
     * @return the usage
     */
    public String[] getUsage() {
        return ArrayUtil.copyOf(usage);
    }

    /**
     * @return the marketingSeasons
     */
    public String[] getMarketingSeasons() {
        return ArrayUtil.copyOf(marketingSeasons);
    }

    /**
     * @return the imageExpiryDate
     */
    public DateTime getImageExpiryDate() {
        return imageExpiryDate;
    }

    /**
     * @return the latestFileDownloadPartitionUrl
     */
    public String getLatestFileDownloadPartitionUrl() {
        return latestFileDownloadPartitionUrl;
    }

    /**
     * @return the productSize
     */
    public String getProductSize() {
        return productSize;
    }

    /**
     * @return the modelHeight
     */
    public String getModelHeight() {
        return modelHeight;
    }

    /**
     * @return the modelWaist
     */
    public String getModelWaist() {
        return modelWaist;
    }

    /**
     * @return the modelChest
     */
    public String getModelChest() {
        return modelChest;
    }

    /**
     * @return the technicalSize
     */
    public String getTechnicalSize() {
        return technicalSize;
    }

}
