package com.adidas.dam.marvin.domain;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Entity {

	public static final String ID = "Id";
	public static final String CREATED_ON = "CreatedOn";
	public static final String TYPE = "Type";
	public static final String MODIFIED_ON = "ModifiedOn";
	public static final String BRAND = "Brand";
	public static final String METADATA_UPDATED = "MetadataUpdated";
	public static final String CONF_EXPIRY_DATE = "ConfidentialityExpiryDate";
	public static final String CONTENT_TYPE = "ContentType";
	public static final String CONFIDENTIAL = "Confidential";
	public static final String IMAGE_LAUNCH_DATE = "ImageLaunchDate";

	@JsonProperty(ID)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("string")
	private String id;

	@JsonProperty(CREATED_ON)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("DateTime")
	private DateTime createdOn;

	@JsonProperty(TYPE)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("string")
	private String type;
	
	@JsonProperty(MODIFIED_ON)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("DateTime")
	private DateTime modifiedOn; 
	
	@JsonProperty(BRAND)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("string")
	private String brand;
	
	@JsonProperty(METADATA_UPDATED)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("DateTime")
	private DateTime metadataUpdated;
	
	@JsonProperty(CONF_EXPIRY_DATE)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("DateTime")
	private DateTime confidentialExpiry;

	@JsonProperty(CONTENT_TYPE)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("string")
	private String contentType;
	
	@JsonProperty(CONFIDENTIAL)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("boolean")
	private boolean confidentialFlag;
	
	@JsonProperty(IMAGE_LAUNCH_DATE)
	@Filterable
	@Orderable
	@Selectable
	@ODataType("DateTime")
	private DateTime imageLaunchDate;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the createdOn
	 */
	public DateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the modifiedOn
	 */
	public DateTime getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @return the metadataUpdated
	 */
	public DateTime getMetadataUpdated() {
		return metadataUpdated;
	}

	/**
	 * @return the confidentialExpiry
	 */
	public DateTime getConfidentialExpiry() {
		return confidentialExpiry;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @return the confidentialFlag
	 */
	public boolean isConfidentialFlag() {
		return confidentialFlag;
	}

	/**
	 * @return the imageLaunchDate
	 */
	public DateTime getImageLaunchDate() {
		return imageLaunchDate;
	}	
}
