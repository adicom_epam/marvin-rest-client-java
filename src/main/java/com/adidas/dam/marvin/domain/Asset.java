package com.adidas.dam.marvin.domain;

import com.adidas.dam.marvin.util.ArrayUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

// This is perfectly ok since it matches the object being returned by 
// the REST service
@SuppressWarnings("TooManyFields")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Asset extends Entity {

    public static final String MARKETS = "Markets";
    public static final String SPORTS_CATEGORIES = "SportsCategories";
    public static final String ASSET_NAME = "AssetName";
    public static final String CONTACT = "Contact";
    public static final String LATEST_FILE_SIZE_BYTES = "LatestFileSizeBytes";
    public static final String LATEST_FILE_VERSION_NUMBER = "LatestFileVersionNumber";
    public static final String LATEST_FILE_COLOR_MODE = "LatestFileColorMode";
    public static final String LATEST_FILE_RESOLUTION_DPI = "LatestFileResolutionDpi";
    public static final String LATEST_FILE_WIDTH_PX = "LatestFileWidthPx";
    public static final String LATEST_FILE_HEIGHT_PX = "LatestFileHeightPx";
    public static final String LATEST_FILE_COLOR_PROFILE = "LatestFileColorProfile";
    public static final String LATEST_FILE_FORMAT = "LatestFileFormat";
    public static final String LATEST_FILE_NAME = "LatestFileName";
    public static final String LATEST_FILE_UPDATED = "LatestFileUpdated";
    public static final String LATEST_FILE_ID = "LatestFileId";
    public static final String LATEST_FILE_EXIF_SOFTWARE = "LatestFileExifSoftware";
    public static final String LATEST_FILE_DOWNLOAD_URL = "LatestFileDownloadUrl";

    @JsonProperty(MARKETS)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] markets;

    @JsonProperty(SPORTS_CATEGORIES)
    @Filterable
    @Selectable
    @ODataType("string")
    private String[] sportsCategories;

    @JsonProperty(ASSET_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String assetName;

    @JsonProperty(CONTACT)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String contact;

    @JsonProperty(LATEST_FILE_SIZE_BYTES)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("int")
    private long latestFileSizeBytes;

    @JsonProperty(LATEST_FILE_VERSION_NUMBER)
    @Selectable
    @ODataType("int")
    private long latestFileVersionNumber;

    @JsonProperty(LATEST_FILE_COLOR_MODE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileColorMode;

    @JsonProperty(LATEST_FILE_RESOLUTION_DPI)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("float")
    private double latestFileResolutionDpi;

    @JsonProperty(LATEST_FILE_WIDTH_PX)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("int")
    private int latestFileWidthPx;

    @JsonProperty(LATEST_FILE_HEIGHT_PX)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("int")
    private int latestFileHeightPx;

    @JsonProperty(LATEST_FILE_COLOR_PROFILE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileColorProfile;

    @JsonProperty(LATEST_FILE_FORMAT)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileFormat;

    @JsonProperty(LATEST_FILE_NAME)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileName;

    @JsonProperty(LATEST_FILE_UPDATED)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("DateTime")
    private DateTime latestFileUpdated;

    @JsonProperty(LATEST_FILE_ID)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileId;

    @JsonProperty(LATEST_FILE_EXIF_SOFTWARE)
    @Filterable
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileExifSoftware;

    @JsonProperty(LATEST_FILE_DOWNLOAD_URL)
    @Orderable
    @Selectable
    @ODataType("string")
    private String latestFileDownloadUrl;

    /**
     * @return the markets
     */
    public String[] getMarkets() {
        return ArrayUtil.copyOf(markets);
    }

    /**
     * @return the sportsCategories
     */
    public String[] getSportsCategories() {
        return ArrayUtil.copyOf(sportsCategories);
    }

    /**
     * @return the assetName
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @return the latestFileSizeBytes
     */
    public long getLatestFileSizeBytes() {
        return latestFileSizeBytes;
    }

    /**
     * @return the latestFileVersionNumber
     */
    public long getLatestFileVersionNumber() {
        return latestFileVersionNumber;
    }

    /**
     * @return the latestFileColorMode
     */
    public String getLatestFileColorMode() {
        return latestFileColorMode;
    }

    /**
     * @return the latestFileResolutionDpi
     */
    public double getLatestFileResolutionDpi() {
        return latestFileResolutionDpi;
    }

    /**
     * @return the latestFileWidthPx
     */
    public int getLatestFileWidthPx() {
        return latestFileWidthPx;
    }

    /**
     * @return the latestFileHeightPx
     */
    public int getLatestFileHeightPx() {
        return latestFileHeightPx;
    }

    /**
     * @return the latestFileColorProfile
     */
    public String getLatestFileColorProfile() {
        return latestFileColorProfile;
    }

    /**
     * @return the latestFileFormat
     */
    public String getLatestFileFormat() {
        return latestFileFormat;
    }

    /**
     * @return the latestFileName
     */
    public String getLatestFileName() {
        return latestFileName;
    }

    /**
     * @return the latestFileUpdated
     */
    public DateTime getLatestFileUpdated() {
        return latestFileUpdated;
    }

    /**
     * @return the latestFileId
     */
    public String getLatestFileId() {
        return latestFileId;
    }

    /**
     * @return the latestFileExifSoftware
     */
    public String getLatestFileExifSoftware() {
        return latestFileExifSoftware;
    }

    /**
     * @return the latestFileDownloadUrl
     */
    public String getLatestFileDownloadUrl() {
        return latestFileDownloadUrl;
    }
}
