package com.adidas.dam.marvin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetsResponse {

    @JsonProperty("Count")
    private long count;

    @JsonProperty("NextCursorMark")
    private String nextCursorMark;

    @JsonProperty("Results")
    private List<ProductImageAsset> assets;

    /**
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * @return the next cursor mark
     */
    public String getNextCursorMark() {
        return nextCursorMark;
    }

    /**
     * @return the assets
     */
    public List<ProductImageAsset> getAssets() {
        return assets;
    }

}
