package com.adidas.dam.marvin.client.query;

/**
 * Enum to define possible values to be passed in {@link FileFilter#withAlignment} to
 * define the alignment of the original image if the image is smaller than the requested 
 * canvas.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum Direction {
	TOP_LEFT("TopLeft"),
	TOP_CENTER("TopCenter"),	
	TOP_RIGHT("TopRight"),
	CENTER_LEFT("CenterLeft"),
	CENTER_CENTER("CenterCenter"),
	CENTER_RIGHT("CenterRight"),
	BOTTOM_LEFT("BottomLeft"),
	BOTTOM_CENTER("BottomCenter"),
	BOTTOM_RIGHT("BottomRight"),
	;
	
	private String value;
	
	private Direction(String value) {
		this.value = value;
	}

	/**
	 * Returns the string representation of the chosen instance which can be directly
	 * passed as URL param. 
	 */
	public String toString() {
		return value;
	}
}
