package com.adidas.dam.marvin.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.FileFilter;
import com.adidas.dam.marvin.client.query.FileFormat;
import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.client.query.QueryBuilder;
import com.adidas.dam.marvin.client.query.filter.AndFilter;
import com.adidas.dam.marvin.client.query.filter.AssetFilter;
import com.adidas.dam.marvin.domain.ProductImageAsset;

@Component
public class Application {
		
	private static final Logger LOG = LoggerFactory.getLogger(Application.class);
		
    public static void main(String[] args) throws InterruptedException {
    	MarvinClient client = null;
		try {
			client = MarvinClient.getInstance(args[0], args[1], args[2]);
		} catch (MarvinClientException e) {
			System.out.println("Unable to create Marvin client");
			e.printStackTrace();
			System.exit(1);
		}
		
		FileFormat format = FileFormat.TIF;
		
    	FileFilter imageFilter = FileFilter.create()
					.withFormat(format)
					.withMaxHeight(1000)
					.withMaxWidth(1000)
					.enableClipping();
    	
		Query q = QueryBuilder
			.create()
			.withFilter(new AndFilter(
					new AssetFilter(ProductImageAsset.ARTICLE_NUMBER, Operand.EQUAL, "G17068")
					))
			.build();
			
		List<ProductImageAsset> assets = null;
		try {
			assets = client.getAssetsResponse(q).getAssets();
		} catch (MarvinClientException e) {
			LOG.error("Unable to request assets.", e);
			System.exit(1);
		}
		
		Map<ProductImageAsset, Future<InputStream>> imageStreamMap = new HashMap<ProductImageAsset, Future<InputStream>>();
		for (ProductImageAsset asset : assets) {
			try {
				imageStreamMap.put(asset,
						client.getImageAsync(imageFilter, asset));
			} catch (MarvinClientException e) {
				LOG.error("Unable to download assets.", e);
			}
		}

		int filesToDownload = imageStreamMap.size();
		while (filesToDownload > 0) {
			LOG.info("{} elements in download queue.", filesToDownload);
			for (Entry<ProductImageAsset, Future<InputStream>> entry : imageStreamMap
					.entrySet()) {
				if (entry.getValue().isDone()) {
					InputStream in = null;
					OutputStream out = null;
					try {
						ProductImageAsset asset = entry.getKey();
						in = entry.getValue().get();
						String filename = asset.getLatestFileId() + "." + format;
						out = Files.newOutputStream(Paths.get(args[3], filename), StandardOpenOption.CREATE);
						int count = 0;
						byte[] buff = new byte[8192];
						while ((count = in.read(buff)) > 0) {
							out.write(buff, 0, count);
						}
					} catch (ExecutionException | IOException e) {
						e.printStackTrace();
					} finally {
						if (in != null) {
							try {
								in.close();
							} catch (IOException e) {
								LOG.error("Error while closing Input Stream.",
										e);
							}
						}
						if (out != null) {
							try {
								out.close();
							} catch (IOException e) {
								LOG.error("Error while closing Output Stream.",
										e);
							}
						}
						filesToDownload--;
					}
				}
			}
			Thread.sleep(3000);
		}
		LOG.info("All downloads finished.");
		imageStreamMap.clear();
    }
}
