package com.adidas.dam.marvin.client.service;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.FileFilter;

public interface FileService {

	byte[] getFile(String fileId, FileFilter filter) throws MarvinClientException;
		
}
