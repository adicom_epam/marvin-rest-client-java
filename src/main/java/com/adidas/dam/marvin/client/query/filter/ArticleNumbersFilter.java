package com.adidas.dam.marvin.client.query.filter;

import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArticleNumbersFilter implements Filter {

	public static int DEFAULT_ARTICLE_NUMBERS_LIMIT = 50;
	private static String FIELD = "ArticleNumbers";
	
	private int articleNumbersLimit;
	private List<String> articleNumbers = new ArrayList<String>();

	public ArticleNumbersFilter(List<String> articleNumbers){
		if (isNotEmpty(articleNumbers)){
			setArticleNumbers(articleNumbers);
		}
		setArticleNumbersLimit(DEFAULT_ARTICLE_NUMBERS_LIMIT);
	}

	public int getArticleNumbersLimit() {
		return articleNumbersLimit;
	}
	public void setArticleNumbersLimit(int articleNumbersLimit) {
		this.articleNumbersLimit = articleNumbersLimit;
	}

	public List<String> getArticleNumbers() {
		return articleNumbers;
	}
	public void setArticleNumbers(List<String> articleNumbers) {
		this.articleNumbers = articleNumbers;
	}

	@Override
    public String compile() {
        StringBuffer result = new StringBuffer(FIELD + "/any(a: ");
        for (Iterator<String> it = getArticleNumbers().iterator(); it.hasNext();) {
        	result.append(format("a eq '%s'", it.next()));
            if (it.hasNext()) result.append(" or ");
        }
        result.append(")");
        return result.toString();
    }

}
