package com.adidas.dam.marvin.client.query;

/**
 * Enum defining valid Color Profiles to be used by generating the
 * resulting image. Valid values are:
 * <ul>
 * <li>AdobeRGB1998 defined by {@code ADOBE_RGB_1998} usually used in screen media</li>
 * <li>CoatedGRACol2006 defined by {@code COATED_GRACOL_2006} usually used in print media</li>
 * </ul>
 *
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum ColorProfile {
	ADOBE_RGB_1998("AdobeRGB1998"), 
	COATED_GRACOL_2006("CoatedGRACol2006"),
	;
	
	private String value;
	
	private ColorProfile(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the spring representation (official query param value) of
	 * the chosen enum value.
	 */
	public String toString() {
		return value;
	}
}
