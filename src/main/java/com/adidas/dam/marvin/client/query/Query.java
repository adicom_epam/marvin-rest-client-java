package com.adidas.dam.marvin.client.query;

import com.adidas.dam.marvin.client.query.filter.AbstractFilterExpression;
import com.adidas.dam.marvin.client.query.filter.Filter;
import com.adidas.dam.marvin.client.query.filter.OrFilter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Encapsulates different options used to call Marvins REST services which include
 * <ul>
 * <li>format – which should not be overwritten since only JSON is supported at
 * the moment</li>
 * <li>page – used to retrieve the results in pages</li>
 * <li>order – an expression on which field and in which direction the results
 * should be ordered</li>
 * <li>filters – a list of various filter expressions and combinations which
 * allow the results to be restricted to only some values</li>
 * </ul>
 *
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class Query {

    private final Format format;
    private final AbstractOrderExpression order;
    private final Collection<Filter> filters;
    private final boolean reshooting;
    private final String cursor;
    private final Integer top;

    /**
     * Default constructor that directly sets all necessary values.
     * Should not be called directly. Consider building the {@link Query} using
     * the {@link QueryBuilder}.
     *  @param format     Format in which the answer should be send back.
     * @param order      Order expression to define direction and field which should
 *                   be applied.
     * @param filters    Filters that should be used. Multiple filters will be
     * @param reshooting Reshooting parameter that should be used
     * @param cursor Cursor parameter that should be used
     * @param top Top parameter that should be used
     */
    public Query(
            final Format format,
            final AbstractOrderExpression order,
            final Collection<Filter> filters,
            final Boolean reshooting,
            final String cursor,
            final Integer top) {
        this.format = format;
        this.order = order;
        this.filters = filters;
        this.reshooting = reshooting;
        this.cursor = cursor;
        this.top = top;
    }

    /**
     * Builds all query params which might then be combined by just concating them
     * using an &amp; symbol.
     *
     * @return array of query params.
     */
    public String[] buildParams() {
        final Map<String, String> query = new HashMap<>();

        Filter filter;
        if (filters != null) {
            if (filters.size() > 1) {
                filter = new OrFilter(filters.toArray(new Filter[filters.size()]));
            } else {
                filter = filters.iterator().next();
            }
            query.put(AbstractFilterExpression.FILTER_PREFIX, filter.compile());
        }

        if (order != null) {
            query.putAll(order.build());
        }

        if (format != null) {
            query.putAll(format.build());
        }

        if (reshooting) {
            query.put("reshooting", Boolean.toString(reshooting));
        }

        if (cursor != null) {
            query.put("$cursor", cursor);
        }

        if (top != null) {
            query.put("$top", top.toString());
        }

        final Set<String> queryParams = new HashSet<String>();
        for (Entry<String, String> entry : query.entrySet()) {
            queryParams.add(entry.getKey() + "=" + entry.getValue());
        }

        return queryParams.toArray(new String[0]);
    }

    /**
     * Builds ready to be appended query string by calling internally {@link buildParams}
     * and concatting all values with an "&amp;" and prefixing it with "?" if non-empty.
     *
     * @return a ready to be appended query string.
     */
    public String buildParamsString() {
        return "?" + StringUtils.arrayToDelimitedString(buildParams(), "&");
    }

    public String toString() {
        return buildParamsString();
    }

}
