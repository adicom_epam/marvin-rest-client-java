package com.adidas.dam.marvin.client.query;

import static com.adidas.dam.marvin.client.query.Format.JSON;

import java.util.HashSet;
import java.util.Set;

import com.adidas.dam.marvin.client.query.filter.Filter;

/**
 * Helps creating {@link Query} objects that can be passed to {@link MarvinClient}
 * service calls like {@link MarvinClient.getArticles} or
 * {@link MarvinClient.countAssets}.
 *
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class QueryBuilder {

    private final Format format = JSON;
    private AbstractOrderExpression order;
    private Set<Filter> filters;
    private boolean reshooting;
    private String cursor;
    private Integer top;

    /**
     * Static factory method returning a new instance of a {@link QueryBuilder}
     * which allows easy chaining of calls.
     *
     * @return a new instance of {@link QueryBuilder}
     */
    public static QueryBuilder create() {
        return new QueryBuilder();
    }

    /**
     * Adds order information to the {@link Query}.
     *
     * @param order the order expression containing a field and a direction.
     * @return the reference to a {@link QueryBuilder} allowing chaining.
     */
    public QueryBuilder withOrder(final AbstractOrderExpression order) {
        this.order = order;
        return this;
    }

    /**
     * Adds a Filter to this query. Multiple calls to this method
     * will combine those filters using an or expression.
     *
     * @param filter the Filter to be added to the query.
     * @return the reference to a {@link QueryBuilder} allowing chaining.
     */
    public QueryBuilder withFilter(final Filter filter) {
        if (filter == null) return this;
        if (filters == null) {
            filters = new HashSet<Filter>();
        }
        filters.add(filter);
        return this;
    }

    /**
     * Adds a Reshooting parameter to this query
     *
     * @param reshooting the Reshooting to be added to the query.
     * @return the reference to a {@link QueryBuilder} allowing chaining.
     */
    public QueryBuilder withReshooting(final boolean reshooting) {
        this.reshooting = reshooting;
        return this;
    }

    /**
     * Adds a cursor parameter to this query
     *
     * @param cursor the Cursor to be added to the query.
     * @return the reference to a {@link QueryBuilder} allowing chaining.
     */
    public QueryBuilder withCursor(final String cursor) {
        this.cursor = cursor;
        return this;
    }

    /**
     * Adds a cursor parameter to this query
     *
     * @param top the Top to be added to the query.
     * @return the reference to a {@link QueryBuilder} allowing chaining.
     */
    public QueryBuilder withTop(final Integer top) {
        this.top = top;
        return this;
    }

    /**
     * Constructs a new {@link Query} object using the given values.
     *
     * @return a new Query object containing the passed values.
     */
    public Query build() {
        return new Query(format,
                order,
                filters,
                reshooting,
                cursor,
                top);
    }

}
