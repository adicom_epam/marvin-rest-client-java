package com.adidas.dam.marvin.client;

import java.util.List;
import java.util.concurrent.Executor;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

/**
 * Configures the Spring Boots Application context. Configured to allow
 * {@link EnableAutoConfiguration}, {@link EnableAsync} and execute a 
 * {@link ComponentScan} during startup.
 * 
 * Also injects a {@link RestTemplate} that allows retrieval of raw byte 
 * streams as well as authenticating against Marvin through NTLM.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
@Configuration
@EnableAutoConfiguration
@EnableAsync
@ComponentScan
public class ClientConfiguration implements AsyncConfigurer {
	
	@Value("${marvin.username}")
	private String username;
	
	@Value("${marvin.password}")
	private String password;

	/**
	 * Injects a {@link RestTemplate} allowing to retrieve raw bytes as well
	 * as authenticating via NTLM. 
	 * 
	 * @param messageConverters	The container default configured {@link HttpMessageConverter}s
	 * to be extended.  
	 * @return a RestTemplate that is able of retrieving raw byte streams and authenticating
	 * via NTLM.
	 */
    @Bean
    public RestTemplate getRestTemplate(List<HttpMessageConverter<?>> messageConverters) {
    	messageConverters.add(new ByteArrayHttpMessageConverter());
    	final CredentialsProvider credsProv = new BasicCredentialsProvider();
    	credsProv.setCredentials(
    			AuthScope.ANY, 
    			new NTCredentials(username, password, null, null)
    	);
    	final HttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProv).build();
    	final RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
    	restTemplate.setMessageConverters(messageConverters);
    	return restTemplate;
    }

    /**
     * Injects a {@link ThreadPoolExecutor} to be used by Springs {@link Async} annotation. 
     * Configured to use 5 threads at a time, allow to queue up to 5,000 items and wait for 
     * completion on shutdown.
     */
	@Override
	public Executor getAsyncExecutor() {
		final ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
		tpte.setCorePoolSize(5);
		tpte.setMaxPoolSize(5);
		tpte.setQueueCapacity(5000);
		tpte.setAllowCoreThreadTimeOut(true);
		tpte.setWaitForTasksToCompleteOnShutdown(true);
		tpte.setAwaitTerminationSeconds(5);
		tpte.setKeepAliveSeconds(15);
		tpte.initialize();
		return tpte;
	}
}
