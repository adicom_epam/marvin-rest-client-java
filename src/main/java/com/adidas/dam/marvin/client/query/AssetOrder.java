package com.adidas.dam.marvin.client.query;

import com.adidas.dam.marvin.domain.ProductImageAsset;

/**
 * Concrete and {@link ProductImageAsset} type safe implementation of an Order Expression
 * which can be used to order {@link MarvinClient#getAssets} results.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class AssetOrder extends AbstractOrderExpression {

	/**
	 * Default constructor expects the Field on which to order as well as a
	 * direction defined by an instance of {@link Order} enum. E.g.:
	 * 
	 * <pre>
	 * {@code
	 * Query query = QueryBuilder
	 *    .create()
	 *    .withOrder(new AssetOrder(
	 *       ProductImageAsset.ARTICLE_NAME, Order.DESC))
	 *    .build();
	 * }
	 * </pre>
	 * 
	 * @param orderField Name of the field on which to order. 
	 * @param order Direction of ordering defined by either {@link Order.ASC} or {@link Order.DESC}
	 */
	public AssetOrder(String orderField,
			Order order) {
		super(ProductImageAsset.class, orderField, order);
	}

}
