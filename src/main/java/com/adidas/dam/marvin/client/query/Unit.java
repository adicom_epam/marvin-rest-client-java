package com.adidas.dam.marvin.client.query;

/**
 * Definition of Units that can be used to define values
 * passed to the {@link FileFilter} expression.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum Unit {
	PIXEL, PERCENT;
	
	public static final String UNIT_PREFIX = "units";
	
	/**
	 * Returns the Spring representation of the unit.
	 */
	public String toString() {
		return name().toLowerCase();
	}
}
