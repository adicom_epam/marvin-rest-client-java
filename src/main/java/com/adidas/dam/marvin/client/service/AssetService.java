package com.adidas.dam.marvin.client.service;

import java.util.List;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.domain.AssetsResponse;

public interface AssetService {

	AssetsResponse find(Query query)
			throws MarvinClientException;
	
	long count(Query query)
			throws MarvinClientException;

	AssetsResponse findByArticleNumbers(List<String> articleNumbers)
			throws MarvinClientException;

	AssetsResponse findUpdatedByArticleNumbers(List<String> articleNumbers, String lastUpdatedDate)
			throws MarvinClientException;

}
