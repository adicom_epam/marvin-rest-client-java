package com.adidas.dam.marvin.client.query.filter;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StatusFilter implements Filter {
    private static final String STATUS_FIELD = "Status";
    private List<String> statuses = new ArrayList<String>();

    public StatusFilter(final List<String> statuses) {
        this.statuses = statuses;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(final List<String> statuses) {
        this.statuses = statuses;
    }

    @Override
    public String compile() {
        StringBuffer result = new StringBuffer("(");
        for (Iterator<String> it = getStatuses().iterator(); it.hasNext();) {
            result.append(format(STATUS_FIELD + " eq '%s'", it.next()));
            if (it.hasNext()) {
                result.append(" or ");
            }
        }
        result.append(")");
        return result.toString();
    }
}
