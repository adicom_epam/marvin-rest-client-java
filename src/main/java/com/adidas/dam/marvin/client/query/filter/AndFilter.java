package com.adidas.dam.marvin.client.query.filter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;

public class AndFilter implements Filter {

	private final Set<Filter> filters;
	
	public AndFilter(final Filter... filters) {
		if (filters.length == 0) {
			throw new IllegalArgumentException("At least one filter expression has to be passed.");
		}
		
		this.filters = new HashSet<Filter>();
		this.filters.addAll(Arrays.asList(filters));
	}
		
	@Override
	public String compile() {
		final Set<String> filterStrings = new HashSet<String>();
		for (Filter filter : filters) {
			filterStrings.add(filter.compile());
		}
		return "(" + StringUtils.collectionToDelimitedString(filterStrings, " and ") + ")";
	}

}
