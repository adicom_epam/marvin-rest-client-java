package com.adidas.dam.marvin.client.service;

import static com.adidas.dam.marvin.client.query.Operand.GREATER_OR_EQUAL;
import static com.adidas.dam.marvin.domain.Entity.MODIFIED_ON;
import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.client.query.QueryBuilder;
import com.adidas.dam.marvin.client.query.filter.AndFilter;
import com.adidas.dam.marvin.client.query.filter.ArticleNumbersFilter;
import com.adidas.dam.marvin.client.query.filter.AssetFilter;
import com.adidas.dam.marvin.client.query.filter.Filter;
import com.adidas.dam.marvin.client.query.filter.StatusFilter;
import com.adidas.dam.marvin.domain.AssetsResponse;
import com.adidas.dam.marvin.domain.ProductImageAsset;

@Service
public class AssetServiceImpl implements AssetService {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(AssetServiceImpl.class);

    private static final String ASSET_SERVICE_PATH = "/contents/productimages/assets";

    public static final long MAX_TOP_VALUE = 100;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${marvin.baseUrl}")
    private String baseUrl;

    @Value("#{'${marvin.assets.statuses}'.split(',')}")
    private List<String> assetsStatuses;

    @Override
    public AssetsResponse find(Query query) throws MarvinClientException {
        return fireQuery(query);
    }

    @Override
    public long count(Query query) throws MarvinClientException {
        return fireQuery(query).getCount();
    }

    @Override
    public AssetsResponse findByArticleNumbers(List<String> articleNumbers) throws MarvinClientException {
        AssetsResponse response = new AssetsResponse();
        if (isEmpty(articleNumbers))
            return response;
        int limit = ArticleNumbersFilter.DEFAULT_ARTICLE_NUMBERS_LIMIT;
        int size = articleNumbers.size();

        String nextCursorMark = "*";
        String prevCursor = null;

        for (int i = 0; i < size; i = i + limit) {
            int from = i;
            int to = i + limit;
            if (to > size)
                to = size;
            final Filter articleNumbersFilter = new ArticleNumbersFilter(articleNumbers.subList(from, to));
            final Filter statusFilter = new StatusFilter(assetsStatuses);
            final Filter filter = new AndFilter(articleNumbersFilter, statusFilter);
            final QueryBuilder queryBuilder = QueryBuilder.create().withFilter(filter);

            if (from == 0) {
                final Query getQuery = queryBuilder.withCursor(nextCursorMark).build();
                response = find(getQuery);

                prevCursor = nextCursorMark;
                nextCursorMark = response.getNextCursorMark();
            }

            while (!nextCursorMark.equals(prevCursor)) {
                final Query getQuery = queryBuilder.withCursor(nextCursorMark).build();

                prevCursor = nextCursorMark;
                nextCursorMark = response.getNextCursorMark();

                if (!nextCursorMark.equals(prevCursor)) {
                    final List<ProductImageAsset> assets = find(getQuery).getAssets();
                    response.getAssets().addAll(assets);
                }
            }
        }
        return response;
    }

    @Override
    public AssetsResponse findUpdatedByArticleNumbers(List<String> articleNumbers, String lastUpdatedDate)
            throws MarvinClientException {
        AssetsResponse response = new AssetsResponse();
        if (isEmpty(articleNumbers))
            return response;
        int limit = ArticleNumbersFilter.DEFAULT_ARTICLE_NUMBERS_LIMIT;
        int size = articleNumbers.size();

        String nextCursorMark = "*";
        String prevCursor = null;

        for (int i = 0; i < size; i = i + limit) {
            int from = i;
            int to = i + limit;
            if (to > size)
                to = size;
            final Filter articleNumbersFilter = new ArticleNumbersFilter(articleNumbers.subList(from, to));
            final Filter lastUpdatedFilter = new AssetFilter(MODIFIED_ON, GREATER_OR_EQUAL, lastUpdatedDate);
            final Filter statusFilter = new StatusFilter(assetsStatuses);
            final Filter filter = new AndFilter(articleNumbersFilter, lastUpdatedFilter, statusFilter);
            final QueryBuilder queryBuilder = QueryBuilder.create().withFilter(filter);

            if (from == 0) {
                final Query getQuery = queryBuilder.withCursor(nextCursorMark).build();
                response = find(getQuery);

                prevCursor = nextCursorMark;
                nextCursorMark = response.getNextCursorMark();
            }

            while (!nextCursorMark.equals(prevCursor)) {
                final Query getQuery = queryBuilder.withCursor(nextCursorMark).build();

                prevCursor = nextCursorMark;
                nextCursorMark = response.getNextCursorMark();

                if (!nextCursorMark.equals(prevCursor)) {
                    final List<ProductImageAsset> assets = find(getQuery).getAssets();
                    response.getAssets().addAll(assets);
                }
            }
        }
        return response;
    }

    private AssetsResponse fireQuery(Query query) throws MarvinClientException {
        StringBuffer url = new StringBuffer(baseUrl);
        url.append(ASSET_SERVICE_PATH);
        url.append(query.buildParamsString());
        try {
            return restTemplate.getForObject(url.toString(), AssetsResponse.class);
        } catch (Exception e) {
            LOG.error(format("Couldn't execute Query for Assets", ""), e);
            throw new MarvinClientException(format("Couldn't execute Query for Assets", ""), e);
        }
    }

}
