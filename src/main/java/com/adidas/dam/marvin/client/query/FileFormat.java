package com.adidas.dam.marvin.client.query;

/**
 * Definiton of supported file formats. Valid values are:
 * <ul>
 * <li>TIF: tiff image including LZW compression</li>
 * <li>JPG: simple jpeg file. Ensure setting quality</li>
 * <li>PNG: modern web based format allowing 24bit color depth as well as transparency.</li>
 * <li>EPS: rather print media centric format which allows embedded JPG previews.</li>
 * <li>PSD: directly editable in Photoshop</li>
 * </ol>
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum FileFormat {
	TIF, JPG, PNG, EPS, PSD, MP4;
	
	public static final String FORMAT_PREFIX = "format";
	
	/**
	 * Returns string representation of the definition which can direclty be used for 
	 * query string construction.
	 */
	public String toString() {
		return name().toLowerCase();
	}
}
