package com.adidas.dam.marvin.client.service;

import static java.lang.String.format;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.FileFilter;

@Service
public class FileServiceImpl implements FileService {

	private static final Logger LOG = LoggerFactory.getLogger(FileServiceImpl.class);
	private static final String FILES_SERVICE_PATH = "/files/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${marvin.baseUrl}")
	private String baseUrl;

	@Override
	public byte[] getFile(String fileId, FileFilter filter) throws RestClientException, MarvinClientException  {
		StringBuffer url = new StringBuffer(baseUrl);
		url.append(FILES_SERVICE_PATH);
		url.append(fileId);
		if (filter != null){
			url.append(filter.buildQueryString());
		}
		try {
			return restTemplate.getForObject(url.toString(), byte[].class);
        } catch (Exception e) {
			LOG.error(format("Couldn't execute Query for Files", ""), e);
			throw new MarvinClientException(format("Couldn't execute Query for Files", ""), e);
        }
	}

}
