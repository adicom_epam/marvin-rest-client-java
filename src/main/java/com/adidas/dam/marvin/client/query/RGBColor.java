package com.adidas.dam.marvin.client.query;

import com.adidas.dam.marvin.client.query.filter.Filter;

/**
 * Defines colors to be used as background fill.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class RGBColor implements Filter {

	public static final int CHANNEL_MIN_VALUE = 0;
	public static final int CHANNEL_MAX_VALUE = 255;
	
	private static final String BACKGROUND_COLOR_PREFIX = "background";
	
	private int red;
	private int green;
	private int blue;
	private boolean transparent;
	
	/**
	 * Default (no-param) constructor returning a RGBColor object representing
	 * a white color. Similar to calling 
	 * <pre>
	 * {@code
	 * RGBColor white = new RGBColor(255, 255, 255);
	 * }
	 * </pre>
	 */
	public RGBColor() {
		this(CHANNEL_MAX_VALUE, CHANNEL_MAX_VALUE, CHANNEL_MAX_VALUE);
	}
	
	/**
	 * Constructor allowing to define a "transparent" background color.
	 * If true is passed as param construct "transparent" RGBcolor. If false is
	 * passed a white RGBColor is returned similar to calling the default
	 * constructor.
	 * 
	 * @param transparent should the color be transparent or not.
	 */
	public RGBColor(boolean transparent) {
		if (transparent) {
			this.transparent = true;
		} else {
			setRed(CHANNEL_MAX_VALUE);
			setGreen(CHANNEL_MAX_VALUE);
			setBlue(CHANNEL_MAX_VALUE);
		}
	}
	
	/**
	 * Constructor allowing to define a value for each color channel 
	 * allowing values for each channel from 0 to 255.
	 * 
	 * @param red the amount of red the final color should contain.
	 * @param green the amount of green the final color should contain.
	 * @param blue the amount of blue the final color should contain.
	 */
	public RGBColor(int red, int green, int blue) {
		setRed(red);
		setGreen(green);
		setBlue(blue);
	}
	
	/**
	 * Sets the red amount executing a boundary check first.
	 * 
	 * @param red the amount of red the final color should contain.
	 */
	private void setRed(int red) {
		checkBoundaries(red);
		this.red = red;
	}
	
	/**
	 * Sets the green amount executing a boundary check first.
	 * 
	 * @param green the amount of green the final color should contain.
	 */
	private void setGreen(int green) {
		checkBoundaries(green);
		this.green = green;
	}
	
	/**
	 * Sets the blue amount executing a boundary check first.
	 * 
	 * @param blue the amount of blue the final color should contain.
	 */
	private void setBlue(int blue) {
		checkBoundaries(blue);
		this.blue = blue;
	}
	
	/**
	 * Checks passed value against unsigned byte boundaries of 0 as lower
	 * and 255 as upper bound (inclusive).
	 * 
	 * @param value the value to be checked.
	 */
	private void checkBoundaries(int value) {
		if (value < CHANNEL_MIN_VALUE || value > CHANNEL_MAX_VALUE) {
			throw new IllegalArgumentException(
				"Channel values must be within the boundaries of "+ 
					CHANNEL_MIN_VALUE + " and " + CHANNEL_MAX_VALUE + 
					". Actual value was " + value);
		}
		
	}

	/**
	 * Returns the colors in hex representation.
	 * 
	 * @return Color in hex format, e.g. 800A3F
	 */
	private String getHexString() {
		return String.format("%02x%02x%02x", red, green, blue).toUpperCase();
	}
	
	/**
	 * Returns the String representation of this object which is "transparent" if 
	 * color is transparent and hex string otherwise.
	 */
	public String toString() {
		return (transparent ? "Transparent" : getHexString());
	}
	
	/**
	 * Returns a ready to be used query param.
	 */
	@Override
	public String compile() {
		return BACKGROUND_COLOR_PREFIX + "=" + this;
	}
	
	
}
