package com.adidas.dam.marvin.util;

import java.util.Arrays;

public final class ArrayUtil {

    public static final <T> T[] copyOf(T[] original) {
        return (original == null) ? null : Arrays.copyOf(original, original.length);
    }

    private ArrayUtil() {
    }
}
