package com.adidas.dam.marvin.client.query.filter;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.Article;
import com.adidas.dam.marvin.domain.ProductImageAsset;

public class ArticleFilterTest {

	private ArticleFilter filter;
	
	@Test
	public void testCompile1() {
		filter = new ArticleFilter(Article.ARTICLE_NAME, Operand.EQUAL, "Superstar");
		assertThat(filter.compile(), equalTo("(" + Article.ARTICLE_NAME + " eq 'Superstar')"));
	}

	@Test
	public void testCompile2() {
		filter = new ArticleFilter(Article.CONF_EXPIRY_DATE, Operand.LESS_OR_EQUAL, "2014-05-01");
		assertThat(filter.compile(), equalTo("(" + Article.CONF_EXPIRY_DATE + " le DateTime'2014-05-01')"));
	}

	@Test
	public void testCompile3() {
		filter = new ArticleFilter(Article.CONFIDENTIAL, Operand.EQUAL, "true");
		assertThat(filter.compile(), equalTo("(" + Article.CONFIDENTIAL + " eq true)"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCompileWithWrongField() {
		filter = new ArticleFilter(ProductImageAsset.DEVELOPER, Operand.EQUAL, "Superstar");
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCompileNullField() {
		filter = new ArticleFilter(null, Operand.EQUAL, "Superstar");
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCompileNullOperand() {
		filter = new ArticleFilter(Article.ARTICLE_NAME, null, "Superstar");
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCompileNullValue() {
		filter = new ArticleFilter(Article.ARTICLE_NAME, Operand.NOT_EQUAL, null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCompileEmptyValue() {
		filter = new ArticleFilter(Article.ARTICLE_NAME, Operand.NOT_EQUAL, "");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCompileExtraLongValue() {
		String randomStr = RandomStringUtils.random(2049, "abcdefghijklmnopqrstuvwxyz0123456789");
		filter = new ArticleFilter(Article.ARTICLE_NAME, Operand.NOT_EQUAL, randomStr);
		assertThat(filter.compile(), containsString(randomStr));
	}
}
