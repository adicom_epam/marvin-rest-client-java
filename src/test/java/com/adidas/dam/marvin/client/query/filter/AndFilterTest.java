package com.adidas.dam.marvin.client.query.filter;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.Article;

public class AndFilterTest {

	private AndFilter andFilter;
	private ArticleFilter filter1 = new ArticleFilter(Article.ARTICLE_NAME, Operand.EQUAL, "Superstar");
	private static final String FILTER_STR1 = "(" + Article.ARTICLE_NAME + " eq 'Superstar')";
	private ArticleFilter filter2 = new ArticleFilter(Article.CONF_EXPIRY_DATE, Operand.GREATER_THAN, "2014-10-01");
	private static final String FILTER_STR2 = "(" + Article.CONF_EXPIRY_DATE + " gt DateTime'2014-10-01')";
	private ArticleFilter filter3 = new ArticleFilter(Article.CONFIDENTIAL, Operand.EQUAL, "true");
	private static final String FILTER_STR3 = "(" + Article.CONFIDENTIAL + " eq true)";

	@Test
	public void testCompile() {
		andFilter = new AndFilter(filter1, filter2, filter3);
		assertThat(andFilter.compile(), allOf(
				containsString(FILTER_STR1),
				containsString(FILTER_STR2),
				containsString(FILTER_STR3)
				));
		assertThat(andFilter.compile().split("and").length, is(3));
	}
	
	@Test
	public void testSingleFilter() {
		andFilter = new AndFilter(filter1);
		assertThat(andFilter.compile(), allOf(
				containsString(FILTER_STR1),
				not(containsString("and"))
				));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyFilter() {
		andFilter = new AndFilter();
		andFilter.compile();
	}

}
