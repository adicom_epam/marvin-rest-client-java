package com.adidas.dam.marvin.client.query;

import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adidas.dam.marvin.client.query.filter.Filter;

public class QueryTest {

	@Mock
	private AbstractOrderExpression order;
	
	@Mock
	private Filter filter;

	@Mock
	private Filter filter2;

	@Mock
	private Filter filter3;

	private Query query;

	@Test
	public void testEmptyBuild() {
		query = QueryBuilder.create().build();
		assertThat(query.buildParamsString(), containsString("format=json"));
	}
	
	@Test 
	public void testWithPage() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create().build();
		query.buildParamsString();
		verify(order, never()).build();
		verify(filter, never()).compile();
	}
	
	@Test
	public void testWithOrder() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create().withOrder(order).build();
		query.buildParamsString();
		verify(order).build();
		verify(filter, never()).compile();
	}

	@Test
	public void testWithFilter() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create().withFilter(filter).build();
		query.buildParamsString();
		verify(filter).compile();
		verify(order, never()).build();
	}
	
	@Test
	public void testWithMultipleFilters() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create()
				.withFilter(filter)
				.withFilter(filter2)
				.withFilter(filter3)
				.build();
		query.buildParamsString();
		verify(filter).compile();
		verify(filter2).compile();
		verify(filter3).compile();
		verify(order, never()).build();
	}
	
	@Test
	public void testOrderFilterCombination() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create()
				.withFilter(filter)
				.withOrder(order)
				.build();
		query.buildParamsString();
		verify(order).build();
		verify(filter).compile();
	}

	@Test
	public void testPageFilterCombination() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create()
				.withFilter(filter)
				.build();
		query.toString();
		verify(filter).compile();
		verify(order, never()).build();
	}

	@Test
	public void testPageOrderCombination() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create()
				.withOrder(order)
				.build();
		query.toString();
		verify(order).build();
		verify(filter, never()).compile();
	}
	
	@Test
	public void testAllCombination() {
		MockitoAnnotations.initMocks(this);
		query = QueryBuilder.create()
				.withOrder(order)
				.withFilter(filter)
				.build();
		query.toString();
		verify(order).build();
		verify(filter).compile();
	}

}
